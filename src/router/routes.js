
const routes = [
  {
    path: '/',
    component: () => import('layouts/Enter.vue'),
    children: [
      { path: '', component: () => import('pages/Login.vue') },
      { path: 'login', component: () => import('pages/Login.vue') },
      { path: 'registration', component: () => import('pages/Registration.vue') },
      { path: 'recovery', component: () => import('pages/Recovery.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
