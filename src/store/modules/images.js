
/* function LOG (message) {
  console.log(`#### ${message}`);
} */

export default {
  state: () => ({
    images: {
      title: {
        azi: 'statics/images/title/azi-logo.svg'
      }
    },
    icons: {
      input: {
        letter: {
          gold: 'statics/icons/input/gold/letter.svg'
        },
        keylock: {
          gold: 'statics/icons/input/gold/keylock.svg'
        }
      }
    }
  }),

  mutations: {}, //
  actions: {}, //
  getters: {
    images: (state) => state.images,
    icons: (state) => state.icons
  } //
}
